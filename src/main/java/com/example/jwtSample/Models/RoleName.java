package com.example.jwtSample.Models;

public enum RoleName {
    ROLE_USER,
    ROLE_ADMIN
}
