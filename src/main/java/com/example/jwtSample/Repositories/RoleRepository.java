package com.example.jwtSample.Repositories;

import com.example.jwtSample.Models.Role;
import com.example.jwtSample.Models.RoleName;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByName(RoleName roleName);
}