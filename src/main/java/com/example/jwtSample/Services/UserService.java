package com.example.jwtSample.Services;

import com.example.jwtSample.Models.User;
import com.example.jwtSample.Repositories.UserRepository;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    @Autowired
    private UserRepository userRep;
    
    User findByUsername(String username){
        
        Optional<User> value = userRep.findByUsername(username);
        if(value.isPresent())
            return value.get();
        return null;
    }

}
